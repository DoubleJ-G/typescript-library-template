/**
 * 
 * This takes in two numbers and returns the result of adding them together
 * 
 * @param a First number
 * @param b Second number
 * @returns a + b
 */

export function add(a: number, b: number): number {
    return a + b;
}
