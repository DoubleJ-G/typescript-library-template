import { add } from './add';

describe("add.ts", () => {
    it("adds two numbers", () => {
        expect(add(1, 2)).toBe(3);
    });
})
